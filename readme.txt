As a WH customer I want to place a bet on a English Premier League event

Full scenario is as follows:

  Navigate to http://sports.williamhill.com/betting/en-gb
  Navigate to a Premiership football event
  Select event and place a £0.05 bet for the home team to ‘Win’
  Place bet and assert the odds and returns offered
  Parameterise the betslip stake so any monetary value can be entered


Requirements:

  Use a Page Objects model (http://code.google.com/p/selenium/wiki/PageObjects)

  Use different levels of abstraction (Abstract classes) to represent Actions, Locators, Data and Test. (Similar but additional to the page objects concept.)
  Have the test create a report of the test results
  Use comments to explain the code and the decisions you made.
  Finally, ensure your test can run both on Desktop and emulate a Mobile device (chrome driver)


Addendum + Issues:

  1. A bet cannot be successfully placed without logging in, and even after logging in
  real money have to be taken from an account. This is why I decided to mark
  a success for test scenario when a message is shown:
  "You must be logged in to place a bet, please log in and try again"
  2. http://sports.williamhill.com/betting/en-gb/football page looks different depending
  whether there are some highlighted matches taking place or not. Combined with little
  distinction between tables with matches, this proves very difficult to find the proper
  Home team match (highlighted bets do not tell you whether you are betting on Home win
  Away win or a Draw).
  3. Sometimes one or two matches (possibly more) are greyed out and cannot be clicked on.
  There is no visible indication why. This might be connected to odds or time until end of
  match?


Troubleshooting tips:

  Start by issuing maven clean install command:
    mvn clean install
  When running from idea command might be helpful in case of maven project configuration issues
    mvn idea:idea

