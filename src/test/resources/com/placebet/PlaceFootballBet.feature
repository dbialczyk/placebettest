Feature: Visit William Hill betting subpage and place a bet

  Scenario: On William Hill homepage place a bet on Football Home team

    Given I want to place a bet of "0.05" pounds
    And I assert the odds are "10/11"
    When I click on Football navigation
    Then A sub page address should read "http://sports.williamhill.com/betting/en-gb/football"
    When I click on first Home team
    And I place my bet
    Then The odds should match my assertion
    And An error should pop up as I am not logged in