package com.placebet.steps;

import com.placebet.pageobjects.TestPage;
import cucumber.annotation.en.Given;
import cucumber.annotation.en.When;
import cucumber.annotation.en.Then;
import cucumber.annotation.Before;
import cucumber.annotation.After;

public class VisitTestPageMobileStepDefs extends MobileChromeBrowserTest {
    private String url;
    public TestPage homepage;


    @Given("^Web Page address for mobile \"([^\"]*)\"$")
    public void Web_Page_address_for_mobile(String url) {
        this.url = url;
    }

    @When("^I visit this adress on chromedriver mobile$")
    public void I_visit_this_adress_on_chromedriver_mobile() {
        this.InitDriver();
        homepage = new TestPage(driver, url);
    }

    @Then("^Page title should read \"([^\"]*)\" on mobile$")
    public void Page_title_should_read_on_mobile(String title) {
        homepage.PageTitleMatches(title);
        this.TearDown();
    }
}
