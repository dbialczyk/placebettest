package com.placebet.steps;

import com.placebet.pageobjects.TestPage;
import cucumber.annotation.en.Given;
import cucumber.annotation.en.When;
import cucumber.annotation.en.Then;
import cucumber.annotation.Before;
import cucumber.annotation.After;


import org.openqa.selenium.WebDriver;

public class VisitTestPageStepDefs extends DesktopChromeBrowserTest {
    private String url;
    public TestPage homepage;

    @Given("^Web Page address \"([^\"]*)\"$")
    public void Web_Page_address(String url) {
        this.url = url;
    }

    @When("^I visit this adress$")
    public void I_visit_this_adress() {
        this.InitDriver();
        homepage = new TestPage(driver, url);
    }

    @Then("^HomePage title should read \"([^\"]*)\"$")
    public void HomePage_title_should_read(String title) {
        homepage.PageTitleMatches(title);
        this.TearDown();
    }
}
