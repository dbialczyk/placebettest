package com.placebet.steps;

import com.placebet.pageobjects.BetPage;
import cucumber.annotation.en.Given;
import cucumber.annotation.en.When;
import cucumber.annotation.en.Then;

import static org.junit.Assert.assertTrue;

public class PlaceFootballBetStepDefs extends DesktopChromeBrowserTest {
    private BetPage betPage;
    private float betAmount;
    private String expectedOdds;
    private String foundOdds;

    @Given("^I want to place a bet of \"([^\"]*)\" pounds$")
    public void I_want_to_place_a_bet_of_pounds(String betAmount) {
        this.betAmount = Float.parseFloat(betAmount);
    }

    @Given("^I assert the odds are \"([^\"]*)\"$")
    public void I_assert_the_odds_are_to(String expectedOdds) {
        this.expectedOdds = expectedOdds;
    }

    @When("^I click on Football navigation$")
    public void I_click_on_Football_navigation() {
        this.InitDriver();
        betPage = new BetPage(driver);
        betPage.GoToFootball();
    }

    @Then("^A sub page address should read \"([^\"]*)\"$")
    public void A_sub_page_address_should_read(String expectedAddress) {
        betPage.PageAddressMatches(expectedAddress);
    }

    @When("^I click on first Home team$")
    public void I_click_on_first_Home_team() {
        this.foundOdds = betPage.SelectFirstHomeTeamAndReturnOdds();
        betPage.PlaceBet(betAmount);
    }

    @When("^I place my bet$")
    public void I_place_my_bet() {
        assertTrue ( "An error message is present after trying to place a bet", betPage.IsPlaceBetErrorPresent() );
    }

    @Then("^The odds should match my assertion$")
    public void The_odds_should_match_my_assertion() {
        assertTrue ("Odds expected should match odds found in bet field", expectedOdds.equals(foundOdds) );
    }

    @Then("^An error should pop up as I am not logged in$")
    public void An_error_should_pop_up_as_I_am_not_logged_in() {
        this.TearDown();
    }



}