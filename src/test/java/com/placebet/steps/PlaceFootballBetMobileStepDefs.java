package com.placebet.steps;

import com.placebet.pageobjects.BetPageMobile;
import cucumber.annotation.en.Given;
import cucumber.annotation.en.When;
import cucumber.annotation.en.Then;

import static org.junit.Assert.assertTrue;

public class PlaceFootballBetMobileStepDefs extends MobileChromeBrowserTest {
    private BetPageMobile betPage;
    private float betAmount;
    private String expectedOdds;
    private String foundOdds;


    @Given("^I want to place a bet of \"([^\"]*)\" pounds on mobile$")
    public void I_want_to_place_a_bet_of_pounds_on_mobile(String betAmount) {
        this.betAmount = Float.parseFloat(betAmount);
    }

    @Given("^I assert the odds are \"([^\"]*)\" on mobile$")
    public void I_assert_the_odds_are_on_mobile(String expectedOdds) {
        this.expectedOdds = expectedOdds;
    }

    @When("^I click on Football navigation on mobile$")
    public void I_click_on_Football_navigation_on_mobile() {
        this.InitDriver();
        betPage = new BetPageMobile(driver);
        betPage.GoToFootball();
    }

    @Then("^A sub page address should read \"([^\"]*)\" on mobile$")
    public void A_sub_page_address_should_read_on_mobile(String arg1) {
        this.foundOdds = betPage.SelectFirstHomeTeamAndReturnOdds();
        betPage.GoToBetslip();
        betPage.PlaceBet(betAmount);
    }

    @When("^I click on first Home team on mobile$")
    public void I_click_on_first_Home_team_on_mobile() {
        assertTrue ( "An error message is present after trying to place a bet", betPage.IsPlaceBetErrorPresent() );
    }

    @When("^I place my bet on mobile$")
    public void I_place_my_bet_on_mobile() {
        assertTrue ( "An error message is present after trying to place a bet", betPage.IsPlaceBetErrorPresent() );
    }

    @Then("^The odds should match my assertion on mobile$")
    public void The_odds_should_match_my_assertion_on_mobile() {
        assertTrue ("Odds expected should match odds found in bet field", expectedOdds.equals(foundOdds) );
    }

    @Then("^An error should pop up as I am not logged in on mobile$")
    public void An_error_should_pop_up_as_I_am_not_logged_in_on_mobile()  {
        this.TearDown();
    }

}
