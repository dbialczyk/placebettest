package com.placebet.steps;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class DesktopChromeBrowserTest implements IDriver {
    protected WebDriver driver;

    /***
     * chromedriver should be on PATH, please set this to a chromedriver executable
     * located inside this project as a workaround if it isn't set on PATH
     * //System.setProperty("webdriver.chrome.driver", "/chromedriver");
     */
    public void InitDriver() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
    }

    public void TearDown() {
        driver.quit();
    }
}
