package com.placebet.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class BetPage extends Page {

    public BetPage(WebDriver driver){
        super(driver,  "http://sports.williamhill.com/betting/en-gb");
    }

    public void GoToFootball() {
        driver.findElement(By.linkText("Football")).click();
    }

    /**
     * There either is one div with Matches or and additional div with Match-Highlights
     * As this is a simple scenario and there is little to distinct both tables
     * a simple solution is chosen with path to first Home team present both with
     * highlights table present and without it. It's hard to prepare a more elegant solution
     * without knowing what and when can be displayed.
     */
    public String SelectFirstHomeTeamAndReturnOdds () {
        String homeTeamOmitHighlightXpath = "//*[@id=\"match-highlights\"]/div/div[2]/section/div[2]/div[1]/div[2]/div[1]/button";
        String homeTeamNoHighlightXpath = "//*[@id=\"match-highlights\"]/div/div[1]/section/div[2]/div[1]/div[2]/div[1]/button";
        String homeTeamXpath =  (( driver.findElements(By.xpath(homeTeamOmitHighlightXpath)).size() != 0 ) ?
                homeTeamOmitHighlightXpath : homeTeamNoHighlightXpath );

        return GetOddsAndClickHomeTeam ( homeTeamXpath);
    }

    protected String GetOddsAndClickHomeTeam (String homeTeamXpath) {
        WaitForElementByXpath(homeTeamXpath);

        WebElement homeTeam = driver.findElement(By.xpath(homeTeamXpath));
        String odds = homeTeam.getText();
        homeTeam.click();

        return odds;
    }

    public void PlaceBet (float betAmount) {
        String betFieldXpath = "//*[@id=\"bets-container-singles\"]/div/div/div[2]/span/input";
        WaitForElementByXpath(betFieldXpath);
        driver.findElement(By.xpath(betFieldXpath)).sendKeys(Float.toString(betAmount));
        String betButtonXpath = "//input[@data-ng-click=\"placeBet()\"]";
        driver.findElement(By.xpath(betButtonXpath)).click();
    }

    public boolean IsPlaceBetErrorPresent ()
    {
        String errorBoxXpath = "//*[@id=\"place-bet-error\"]/span";
        return ( driver.findElements(By.xpath(errorBoxXpath)).size() != 0 );

    }
}
