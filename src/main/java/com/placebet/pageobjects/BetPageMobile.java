package com.placebet.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BetPageMobile extends BetPage {


    public BetPageMobile (WebDriver driver){
        super(driver);
    }

    public void GoToBetslip () {
        driver.findElement(By.xpath("//*[@id=\"betslip-btn-toolbar\"]/a/span[2]")).click();
    }
    public void GoToFootball() {
        driver.findElement(By.linkText("Football")).click();

        WaitForElementByXpathCustomTime("//*[@id=\"mobile-betslip-count\"]", 10); // wait for betslip to be updated
    }

    public String SelectFirstHomeTeamAndReturnOdds () {
        String homeTeamOmitHighlightXpath = "//*[@id=\"match-highlights\"]/div/div[2]/section/div[2]/div[2]/div[2]/div[1]/button";
        String homeTeamNoHighlightXpath = "//*[@id=\"match-highlights\"]/div/div[1]/section/div[2]/div[2]/div[2]/div[1]/button";
        String homeTeamXpath =  (( driver.findElements(By.xpath(homeTeamOmitHighlightXpath)).size() != 0 ) ?
                homeTeamOmitHighlightXpath : homeTeamNoHighlightXpath );

        String odds = GetOddsAndClickHomeTeam ( homeTeamXpath);

        WaitForElementByXpathCustomTime("//*[@class=\"toolbar__badge toolbar__badge--fly-in\"]", 10); // wait for betslip to be updated

        return odds;
    }
}
