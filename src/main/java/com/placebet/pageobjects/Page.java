package com.placebet.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;

public abstract class Page {
    private static final int TIMEOUT = 5;
    protected WebDriver driver;
    private String url;

    public Page(WebDriver driver, String baseUrl){
        this.driver = driver;
        this.url = baseUrl;
        driver.get(url);
    }

    /**
     * See if title of the webpage matches the given title
     * This method should be used to verify if new page has
     * opened correctly.
     * @exception WrongPageException if strings don't match
     */
    public void PageTitleMatches(String expectedTitle) throws WrongPageException {
        if (!driver.getTitle().equals(expectedTitle)) {
            throw new WrongPageException("Incorrect page: Expected: " + expectedTitle +
                    " Found: " + (driver.getTitle()) );
        }
    }

    public void PageAddressMatches(String expectedAddress) throws WrongPageException {
        if (!driver.getCurrentUrl().equals(expectedAddress)) {
            throw new WrongPageException("Incorrect page: Expected: " + expectedAddress +
                    " Found: " + (driver.getCurrentUrl()) );
        }
    }

    protected void WaitForElementByXpath (String XpathToElement) {
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUT);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(XpathToElement)));
    }

    protected void WaitForElementByXpathCustomTime (String xpathToElement, int customTime) {
        WebDriverWait wait = new WebDriverWait(driver, customTime);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathToElement)));
    }
}
