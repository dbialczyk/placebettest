Feature: Visit William Hill betting subpage with mobile Chromedriver

  Scenario: Visit William Hill Homepage on Chromedriver mobile

    Given Web Page address for mobile "http://sports.williamhill.com/betting/en-gb"
    When I visit this adress on chromedriver mobile
    Then Page title should read "Online Betting from William Hill - The Home of Betting" on mobile