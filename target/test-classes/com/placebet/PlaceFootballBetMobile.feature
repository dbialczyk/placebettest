Feature: Visit William Hill betting subpage and place a bet on mobile

  Scenario: On William Hill homepage place a bet on Football Home team on mobile

    Given I want to place a bet of "0.05" pounds on mobile
    And I assert the odds are "10/11" on mobile
    When I click on Football navigation on mobile
    Then A sub page address should read "http://sports.williamhill.com/betting/en-gb/football" on mobile
    When I click on first Home team on mobile
    And I place my bet on mobile
    Then The odds should match my assertion on mobile
    And An error should pop up as I am not logged in on mobile